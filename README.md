Mail GPG
========

# Description
Simple implementation of GnuPG to provide email encryption. Uses `hook_mail_alter` to encrypt all outgoing emails including webform submissions.

# Requirements
Mail GPG expects that [GnuPG](https://www.gnupg.org), specifically the `gpg` executable is installed on the server.

# Usage
* Generate key pair (e.g. via Kleopatra)
* Add public key(s) of recipient(s) by executing `gpg --import key.asc` on the server
* Enable module
* Send mails